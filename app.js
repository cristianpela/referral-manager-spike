(function(){
    'use strict';
    angular.module('app',['app.services','app.directives'])
    .controller('appCtrl',['$scope','DataLoader', 'Logger',function($scope, DataLoader, Logger){
        Logger.listen(function(msg){
            $scope.msg=msg;
        });
        $scope.showDetails=function(referral){
            $scope.current=referral;
        };
        DataLoader.get(function(data){
            $scope.data=data;
        });
    }]);
})();