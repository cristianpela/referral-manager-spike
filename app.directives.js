(function(){
    'use strict';
    angular.module('app.directives',[])
    .directive('dirDataRenderer',['DateUtils',function(DateUtils){
        return{
            scope:{item: '='},
            controller: function($scope,$timeout){
                $scope.dateutils=DateUtils;
                $scope.$watch('item',function(){
                    $scope.daterange=DateUtils.range($scope.item.since,$scope.item.next);
                });
            },
            templateUrl: 'dir_datarenderer.html',
            link: function(scope,elem,attrs){
                
            }
        };
    }]);
})();