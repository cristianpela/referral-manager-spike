(function(){
    'use strict';
    angular.module('app.services',[])
    .service('DataLoader',['$http','Logger',function($http,Logger){
        return{
            get: function(callback){
                $http.get('data.json').success(function(response){
                    callback(response.data);
                }).error(function(err){
                    Logger.addMessage("Error reading data: "+err);
                });
            }
        };
    }])
    .service('Logger',['$rootScope',function($rootScope){
        var messages=[],
            MESSAGE_KEY='app-message';
        return {
            getAll: function(){
                return messages;
            },
            addMessage: function(message){
                $rootScope.$broadcast(MESSAGE_KEY,message);
                messages.push(message);
            },
            listen: function(callback){
                $rootScope.$on(MESSAGE_KEY,function(event,msg){
                    callback(msg);
                });
            }
        };
    }])
    .service('DateUtils',function(){
        var FORMAT='yyyy/MM/dd';
        return{
            range:function(start,end){
                var s=Date.parse(start).add(1).days(),
                    e=Date.parse(end).add(1).days(),
                    diff=new TimeSpan(e-s).days,
                    remain=diff%30;
                if(diff%7!=0){
                    diff+=(diff%7)*7-remain;
                }
                var range=[];
                for(var i=0;i<diff;i++)
                    range.push(Date.parse(start).add(i).days().toString(FORMAT));
                return range;
            },
            next:function(days,from){
                var now= Date.parse(from) || Date.today();
                return now.add(days).days().toString(FORMAT);
            }
        }
    });
})();